#!/bin/bash

# Author: Caleb Connolly
# Date  : 04/10/19
# Source: https://gitlab.com/kalube/scripts

while true; do

	if sunwait poll 51.6092184N -3.9821411W | grep -q 'NIGHT'; then
		feh --bg-fill ~/docs/wallpapers/night.jpg
	else
		feh --bg-fill ~/docs/wallpapers/day.jpg
	fi
	sleep 300
done
