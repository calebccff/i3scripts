#!/usr/bin/python3

from ics import Calendar
import requests
from arrow.arrow import Arrow
from time import sleep

def output(next_event):
    print(" " + next_event[0].datetime.strftime("%A, %I:%M%p") + " - " + next_event[1])

if __name__ == "__main__":
    timetable = None
    while True:
        try:
            timetable = requests.get("https://connolly.tech/api/ec2dcb0e88342f01h134397?group=T:A&hide_optional=t")
            break
        except Exception:
            sleep(3)
    c = Calendar(timetable.text)
    events = list(c.timeline.today(None)) #There's a bug in the library, today method expects a parameter but doesn't use it
    today = [[d.begin, d.name] for d in events]
    #print(today)
    next_event = [Arrow.now(), "Nothing on today!"]
    try:
        next_event = today[0]
    except IndexError:
        output(next_event)
        exit(0)
    i = 0
    while next_event[0].timestamp < Arrow.now().timestamp:
        i += 1
        try:
            next_event = today[i]
        except IndexError:
            next_event = [Arrow.now(), "All done today :)"]
    output(next_event)
