#!/bin/bash

if [ ip a | grep -q "mon0" ]; then
	iw dev mon0 del
	iw phy phy0 interface add wlp3s0 type managed
else
	iw dev wlp3s0 del
	iw phy phy0 interface add mon0 type monitor
